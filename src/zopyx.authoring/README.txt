Produce & Publish Authoring Environment
=======================================

The Produce & Publish Authoring Environment is a Plone-based
solution for generating high-quality PDF documents from Plone
content and office content supporting 

- managing editorial content
- conversion assets
- conversion configuration
- conversion and publication

under one hood.

Requirements
------------

- Plone 4.1, 4.2 (untested with Plone 4.3)
- installation of the Produce & Publish server (see documentation)
- currently supports the commercial PDFreactor and PrinceXML as external PDF converter
- support for free PDF generation using PhantomJS upcoming)

.. note:: With ``zopyx.authoring`` Version 2.4 or higher you will need
  to upgrade your Produce & Publish Server to the new ``pp.server``
  implementation.

Documentation
-------------

See http://pythonhosted.org/zopyx.authoring

Source code:
------------

- https://bitbucket.org/ajung/zopyx.authoring/src/master/src/zopyx.authoring

Issue tracker:
--------------

- https://bitbucket.org/ajung/zopyx.authoring/issues


License
-------

``Product & Publish Authoring Environment`` is published under the GNU Public License V2 (GPL 2).

Contact
-------

| ZOPYX Limited
| Hundskapfklinge 33
| D-72074 Tuebingen, Germany
| info@zopyx.com
| www.zopyx.com
| www.produce-and-publish.info
