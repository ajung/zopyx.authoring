.. Produce & Publish Authoring Environment documentation master file, created by
   sphinx-quickstart on Tue Apr 12 15:26:21 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Produce & Publish documentation. 

Produce & Publish is a set
of components for generating high-quality office-documents, PDF documents or e-books.

Produce & Publish Authoring Environment
=======================================

.. toctree::
   :maxdepth: 3

   authoring/installation.rst
   authoring/manage_content.rst
   authoring/conversions_center.rst
   authoring/conversions.rst
   authoring/supplementary.rst

How-Tos
=======
  
.. toctree::
   :maxdepth: 1

   howto/walkthrough.rst
   howto/paste_from_word.rst
   howto/edit_authors.rst
   howto/s5-customization.rst      
   howto/index-listing.rst

Copyright & Credits
===================

.. toctree::
   :maxdepth: 1

   misc/copyright.rst
   misc/credits.rst

