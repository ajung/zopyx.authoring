#####################################################################
# zopyx.authoring
# (C) 2010, ZOPYX Limited, D-72070 Tuebingen. All rights reserved
#####################################################################

import os
import json
import cStringIO
import mimetypes
import PIL.Image
import tempfile
from fs.osfs import OSFS
import yaml

from Products.Five.browser import BrowserView

from pp.core.resources_registry import getResource
from zopyx.authoring.logger import LOG

EXT2TYPE = {
    '.pt' : 'template',
    '.cover' : 'coverpage', 
    '.styles' : 'style',
    '.css': 'style',
    '.jpg' : 'image', 
    '.png' : 'image', 
    '.gif' : 'image', 
    '.hyp' : 'hyphenation', 
    '.otf' : 'font', 
    '.ttf' : 'font', 
    '.calibre' : 'calibre', 
    '.conversion' : 'conversion', 
}

EXT2MODE = {
    '.pt' : 'html',
    '.cover' : 'html', 
    '.styles' : 'css',
    '.css': 'css',
    '.jpg' : None, 
    '.png' : None,
    '.gif' : None, 
    '.hyp' : None, 
    '.otf' : None, 
    '.ttf' : None, 
    '.calibre' : 'text', 
    '.conversion' : 'yaml', 
}

class AuthoringTemplateView(BrowserView):

    def __init__(self, context, request):
        self.request = request
        self.context = context

    def _check_yaml(self, yaml_content):

        resource_id = self.context.getResourceOnFilesystem()
        resource = getResource(resource_id)
        resource_files = resource.fslayer.listdir()

        errors = []

        try:
            doc = yaml.load(yaml_content)
        except yaml.scanner.ScannerError:
            errors.append('Unable to parse YAML file')
            return errors

        if doc is None or not isinstance(doc, dict):
            errors.append('Unable to parse YAML file')
            return errors

        template = doc.get('template')
        styles = doc.get('styles')
        cover_front = doc.get('convert-front')
        cover_back = doc.get('convert-back')

        if template is None:
            errors.append(u'No template configured')
        elif not template in resource_files:
            errors.append(u'Template "{}" does not exist'.format(template))

        if styles is None:
            errors.append(u'No styles configured')
        else:
            for style in styles:
                if not style in resource_files:
                    errors.append(u'Style "{}" does not exist'.format(style))

        return errors

    def getEditorContext(self):
        filename = self.request['filename']
        resource_id = self.context.getResourceOnFilesystem()
        resource = getResource(resource_id)

        if filename in self.context.objectIds():
            content = str(self.context[filename].getFile())
        else:
            with resource.fslayer.open(filename, 'rb') as fp:
                content = fp.read()
        basename, ext = os.path.splitext(filename)

        mode = EXT2MODE.get(ext)
        is_image = ext in ('.png', '.jpg', '.gif')
        img_width = img_height = None
        if is_image:
            with resource.fslayer.open(filename, 'rb') as fp:
                img_data = fp.read()
            img_width, img_height = PIL.Image.open(cStringIO.StringIO(img_data)).size

        return dict(content=content,
                    mode=mode,
                    size=resource.fslayer.getsize(filename),
                    mimetype=mimetypes.guess_type(filename)[0],
                    img_width=img_width,
                    img_height=img_height,
                    is_image=is_image)

    def getResourceFiles(self):
        resource = self.context.getResourceOnFilesystem()
        resource = getResource(resource)
        object_ids = self.context.objectIds()
        result = dict()
        for filename in resource.fslayer.walkfiles():
            filename = filename.lstrip('/')        
            basename, ext = os.path.splitext(filename)
            type_ = EXT2TYPE.get(ext)
            if not type_:
                continue

            if not type_ in result:
                result[type_] = []
            result[type_].append({'filename' : filename,
                                  'customized': filename in object_ids
                                })
        return result

    def viewImage(self):

        filename = self.request['filename']
        resource_id = self.context.getResourceOnFilesystem()
        resource = getResource(resource_id)

        base, ext = os.path.split(filename)
        self.request.response.setHeader('content-type', 'image/{}'.format(ext[1:]))
        with resource.fslayer.open(filename, 'rb') as fp:
            image_data = fp.read()
        self.request.response.write(image_data)

    def saveResourceFile(self):
        
        filename = self.request.filename
        content = self.request.content

        if filename.endswith('.conversion'):
            print content
            # parse configuration YAML file
            errors = self._check_yaml(content)
            print errors
            if errors:
                self.request.response.setStatus(400)
                self.request.response.write(json.dumps(errors))
                return 

        if not filename in self.context.objectIds():
            self.context.invokeFactory('File', id=filename)

        file_ = self.context[filename]
        file_.setFile(content)
        file_.reindexObject()

        self.request.response.setStatus(200)
        self.request.response.write('OK')

    def removeResourceFile(self):
        """ Remove a locally customized resource file """

        filename = self.request.filename

        if filename in self.context.objectIds():
            file_ = self.context[filename]
            file_.unindexObject()
            file_.aq_parent.manage_delObjects([filename])

        self.request.response.setStatus(200)
        self.request.response.write('OK')

    def exportResource(self, fslayer=None):
        """ Export resouce to the file system """

        if not fslayer:
            temp_dir = tempfile.mkdtemp()            
            fslayer = OSFS(temp_dir)

        resource_id = self.context.getResourceOnFilesystem()
        resource = getResource(resource_id)
   
        for filename in resource.fslayer.listdir():
            print filename
            if filename in self.context.objectIds():
                content = str(self.context[filename].getFile())
            else:
                with resource.fslayer.open(filename, 'rb') as fp:
                    content = fp.read()

            with fslayer.open(filename, 'wb') as fp:
                fp.write(content)

        return fslayer
