from zope import schema
from zope.interface import Interface

from zopyx.authoring import authoringMessageFactory as _


class IAuthoringCoverPage(Interface):
    """Authoring Cover Page"""

    # -*- schema definition goes here -*-
