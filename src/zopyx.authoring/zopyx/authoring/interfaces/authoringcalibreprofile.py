from zope.interface import Interface
# -*- Additional Imports Here -*-


class IAuthoringCalibreProfile(Interface):
    """Authoring Environment Calibre Profile"""

    # -*- schema definition goes here -*-
