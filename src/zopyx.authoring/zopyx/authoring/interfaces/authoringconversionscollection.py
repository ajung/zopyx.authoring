from zope.interface import Interface
# -*- Additional Imports Here -*-


class IAuthoringConversionsCollection(Interface):
    """Collection for conversion folders"""

    # -*- schema definition goes here -*-
