from zope.interface import Interface
# -*- Additional Imports Here -*-


class IAuthoringPublishedDocument(Interface):
    """Authoring Project Published Document"""

    # -*- schema definition goes here -*-
