from zope.interface import Interface
# -*- Additional Imports Here -*-


class IAuthoringContentAggregator(Interface):
    """AuthoringEnvironment content aggregator"""

    # -*- schema definition goes here -*-
