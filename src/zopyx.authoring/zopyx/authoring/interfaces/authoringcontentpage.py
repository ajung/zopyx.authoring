from zope import schema
from zope.interface import Interface

from zopyx.authoring import authoringMessageFactory as _


class IAuthoringContentPage(Interface):
    """Authoring Content Page"""

    # -*- schema definition goes here -*-
