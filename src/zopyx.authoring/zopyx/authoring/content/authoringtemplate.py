"""Definition of the AuthoringTemplate content type
"""

from zope.interface import implements, directlyProvides

from Products.Archetypes import atapi
from Products.ATContentTypes.content import folder
from Products.ATContentTypes.content import schemata

from zopyx.authoring import authoringMessageFactory as _
from zopyx.authoring.interfaces import IAuthoringTemplate
from zopyx.authoring.config import PROJECTNAME
from Products.CMFCore.ContentTypeRegistry import manage_addRegistry

from pp.core.resources_registry import getResourceNames

AuthoringTemplateSchema = folder.ATFolderSchema.copy() + atapi.Schema((
    atapi.StringField('resourceOnFilesystem',
                      vocabulary='getResourcesOnFilesystem',
                      widget=atapi.SelectionWidget(label=_('label_resource_on_filesystem', default='Resources on filesystem'),
                                                   format='select',
                                                   ),
                      ),

))

AuthoringTemplateSchema['title'].storage = atapi.AnnotationStorage()
AuthoringTemplateSchema['description'].storage = atapi.AnnotationStorage()

schemata.finalizeATCTSchema(
    AuthoringTemplateSchema,
    folderish=True,
    moveDiscussion=False
)

class AuthoringTemplate(folder.ATFolder):
    """Authoring Template"""
    implements(IAuthoringTemplate)

    meta_type = "AuthoringTemplate"
    schema = AuthoringTemplateSchema

    title = atapi.ATFieldProperty('title')
    description = atapi.ATFieldProperty('description')
    
    def getResourcesOnFilesystem(self):
        result = atapi.DisplayList()
        for r in getResourceNames():
            result.add(r, r)
        return result

atapi.registerType(AuthoringTemplate, PROJECTNAME)
