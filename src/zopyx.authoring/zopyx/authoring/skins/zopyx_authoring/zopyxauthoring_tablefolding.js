/*
 * Table overlay
 */


$(document).ready(function() {

    $('table.display-overlay').each(function(i) {
        var table = $(this);
        var id = table.attr('id');
        caption = table.find('caption');
        caption_text = caption.html();
        caption_text = caption_text==null ? '' : caption_text;
        $('<a class="table-popup" href="' + ACTUAL_URL + '/@@show_table?id=' + id +'")">' + caption_text + '</a>').insertBefore(table);
        table.hide();
    });

    $('.table-popup').each(function(i) {
        var anchor = $(this);
        anchor.prepOverlay({
                subtype: 'ajax',
                width: '90%'
            }
        );
    });

})
